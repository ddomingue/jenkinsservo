import serial
import time
import jenkins

arduino = serial.Serial('COM4',9600,timeout=1)
time.sleep(1)


j = jenkins.Jenkins("http://{server}:{port}/") 

flag = 175

while True:
	job_name = 'release-com'
	job_number = int(j.get_job_info(job_name)['nextBuildNumber']) - 1
	build_info = j.get_build_info(job_name, job_number)['result']

	if(build_info == 'SUCCESS'):
		flag = 27
	elif(build_info == 'UNSTABLE'):
		flag = 81
	elif(build_info == 'FAILURE'):
		flag = 135
	else:
		flag = 175

	print("{info} : Flag {flag}".format(info=build_info,flag=flag))
	arduino.write(str(chr(flag)).encode())
	time.sleep(6)
