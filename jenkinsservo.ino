// Sweep
// by BARRAGAN <http://barraganstudio.com> 
// This example code is in the public domain.

// DDO : Modified original sweep to handle incoming angles from serial


#include <Servo.h> 
 
Servo myservo;  // create servo object to control a servo 
int flag = 0;
 
void setup() //setup board on init
{ 
  Serial.begin(9600); //init serial using 9600 baud rate
  Serial.println("Ready");
} 
 
 
void loop() 
{ 
  if(Serial.available()){
    int readValue = Serial.read(); //read int from serial
    Serial.print("Received ");
    Serial.println(readValue);
    
    if(readValue != flag)
    {
      flag = readValue;
      myservo.attach(9); //attach servo to pin 9 (UP)
      myservo.write(flag); //write angle to servo
      delay(750); //wait 750ms for servo to turn
      myservo.detach(); //detach servo (DOWN)
    }
  }
  
  
} 
